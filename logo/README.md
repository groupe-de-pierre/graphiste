Logo officiel du groupe.
Création par DjuDjulien


Couleurs du logo : #F39202 , #31A9E1 et #1C3257

Infos : 
 - les fichiers nommés avec l'extension .min. avant l'extension finale sont les versions compressées

Changelog :
 - 2020/04/01 01:00 (RemRem) corrections graphiques
    - poing plus gros (lisibilité logo en taille réduite)
    - fond blanc pour le poing (pour la transparence)
    
